//
//  ViewController.swift
//  MotivatorsApp
//
//  Created by Bluemm, Steffen on 05.11.19.
//  Copyright © 2019 Bluemm, Steffen. All rights reserved.
//

import UIKit

class MotivatorsViewController: UICollectionViewController {

    /// our collection of motivators
    private var items: [Motivator] = Motivator.generate()
    
    private var longPressGesture: UILongPressGestureRecognizer!
    private var dragGesture: UIPanGestureRecognizer!
    
    fileprivate var draggedItem: MotivatorCell?
    fileprivate var dragVerticalReference: CGFloat = 0
    fileprivate var draggedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressGesture(gesture:)))
        self.collectionView.addGestureRecognizer(longPressGesture)
        
        dragGesture = UIPanGestureRecognizer(target: self, action: #selector(self.handlePanGesture(gesture:)))
        dragGesture.minimumNumberOfTouches = 2
        self.collectionView.addGestureRecognizer(dragGesture)
    }
    
    /// tell the collectionView how many items to display
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    /// tell the collectionView what to display for each item
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        /// ask the collectionView for a reusable cellView by reuse-Id
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MotivatorCell
        
        let item = items[indexPath.item]
        
        cell.setLabel(text: item.name)
        cell.verticalOffset = item.impactOffset
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = items.remove(at: sourceIndexPath.item)
        items.insert(item, at: destinationIndexPath.item)
    }
}

extension MotivatorsViewController {
    @objc func handlePanGesture(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            guard let selectedIndexPath = self.collectionView.indexPathForItem(at: gesture.location(in: self.collectionView)) else {
                break
            }
            draggedIndexPath = selectedIndexPath
            dragVerticalReference = gesture.location(in: self.collectionView).y
            draggedItem = self.collectionView.cellForItem(at: selectedIndexPath) as? MotivatorCell
        case .changed:
            guard let motivator = draggedItem else {
                break
            }
            // TODO: move label in MotivatorCell
            motivator.verticalOffset = gesture.location(in: self.collectionView).y - dragVerticalReference
        case .ended:
            if let indexPath = draggedIndexPath {
                let motivator = items[indexPath.item]
                motivator.impactOffset = gesture.location(in: self.collectionView).y - dragVerticalReference
            }
            dragVerticalReference = 0
            draggedItem = nil
            draggedIndexPath = nil
        default:
            dragVerticalReference = 0
            draggedItem = nil
            draggedIndexPath = nil
        }
    }
    
    @objc func handleLongPressGesture(gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            guard let selectedIndexPath = self.collectionView.indexPathForItem(at: gesture.location(in: self.collectionView)) else {
                break
            }
            self.collectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            self.collectionView.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case .ended:
            self.collectionView.endInteractiveMovement()
        default:
            self.collectionView.cancelInteractiveMovement()
        }
    }
}

