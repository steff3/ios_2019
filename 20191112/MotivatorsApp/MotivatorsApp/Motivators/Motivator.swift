//
//  Motivator.swift
//  MotivatorsApp
//
//  Created by Bluemm, Steffen on 12.11.19.
//  Copyright © 2019 Bluemm, Steffen. All rights reserved.
//

import UIKit

class Motivator {
    let type: MotivatorType
    let name: String
    var impactOffset: CGFloat = 0.0
    var impactIndex: UInt8 = 0
    
    init(type: MotivatorType) {
        self.type = type
        name = type.rawValue.capitalized
    }
}

extension Motivator {
    static func generate() -> [Motivator] {
        let types = MotivatorType.allCases
        
        var motivators: [Motivator]
        motivators = types.map { return Motivator(type: $0) }
        
        return motivators
    }
}
