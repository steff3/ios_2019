import Foundation

public enum MotivatorType: String, CaseIterable {
    case acceptance, curiosity, freedom, status, goal, honor, order, mastery, power, relatedness
}
