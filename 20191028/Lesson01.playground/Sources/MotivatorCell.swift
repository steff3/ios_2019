import UIKit

///A custom derivation of UICollectionViewCell to implement a cellView for a single moving motivator
public class MotivatorCell: UICollectionViewCell {
    private weak var textLabel: UILabel?
    
    public var verticalOffset: CGFloat = 0 {
        didSet {
            centerConstraint.constant = verticalOffset
            setNeedsUpdateConstraints()
        }
    }
    
    fileprivate weak var centerConstraint: NSLayoutConstraint!
    
    public override init(frame: CGRect) {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .white
        label.backgroundColor = .blue
        label.translatesAutoresizingMaskIntoConstraints = false
        
        super.init(frame: frame)
        
        self.addSubview(label)
        textLabel = label
        
        self.setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// we do some auto-layout setup here ...
    /// a bit advanced ... maybe ...
    /// principally we align a textLabel (UILabel) to the size of the cellView ... no magic ....
    func setupConstraints() {
        if let textLabel = textLabel {
            let center = textLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: verticalOffset)
            let constraints = [
                textLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                textLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),
                textLabel.heightAnchor.constraint(equalTo: self.widthAnchor),
                center
            ]
            
            NSLayoutConstraint.activate(constraints)
            centerConstraint = center
        }
    }
    
    /// write text to the textLabel as needed
    public func setLabel(text: String) {
        self.textLabel?.text = text
    }
}

