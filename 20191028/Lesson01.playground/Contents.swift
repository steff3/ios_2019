import UIKit
import PlaygroundSupport



///
///     Our custom UICollectionViewController class
///     - hold a collection of motivators
///     - instruct the UICollectionView to display them correctly
///
class MotivatorsViewController: UICollectionViewController {
    /// our collection of motivators
    private var items: [MotivatorType] = MotivatorType.allCases
    
    private var longPressGesture: UILongPressGestureRecognizer!
    private var dragGesture: UIPanGestureRecognizer!
    
    fileprivate var draggedItem: MotivatorCell?
    fileprivate var dragVerticalReference: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// tell the collectionView which type of cellView to use, as well as the reuse-Id
        self.collectionView.register(MotivatorCell.self, forCellWithReuseIdentifier: "Cell")
        
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressGesture(gesture:)))
        self.collectionView.addGestureRecognizer(longPressGesture)
        
        dragGesture = UIPanGestureRecognizer(target: self, action: #selector(self.handlePanGesture(gesture:)))
        self.collectionView.addGestureRecognizer(dragGesture)
    }
    
    /// tell the collectionView how many items to display
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    /// tell the collectionView what to display for each item
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        /// ask the collectionView for a reusable cellView by reuse-Id
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MotivatorCell
        //cell.backgroundColor = .blue
        
        let item = items[indexPath.item]
        
        cell.setLabel(text: item.rawValue.capitalized)
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = items.remove(at: sourceIndexPath.item)
        items.insert(item, at: destinationIndexPath.item)
    }
}

extension MotivatorsViewController {
    @objc func handlePanGesture(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            guard let selectedIndexPath = self.collectionView.indexPathForItem(at: gesture.location(in: self.collectionView)) else {
                break
            }
            
            dragVerticalReference = gesture.location(in: self.collectionView).y
            draggedItem = self.collectionView.cellForItem(at: selectedIndexPath) as? MotivatorCell
        case .changed:
            guard let motivator = draggedItem else {
                break
            }
        // TODO: move label in MotivatorCell
            motivator.verticalOffset = gesture.location(in: self.collectionView).y - dragVerticalReference
        case .ended:
            dragVerticalReference = 0
            draggedItem = nil
        default:
            dragVerticalReference = 0
            draggedItem = nil
        }
    }
    
    @objc func handleLongPressGesture(gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            guard let selectedIndexPath = self.collectionView.indexPathForItem(at: gesture.location(in: self.collectionView)) else {
                break
            }
            self.collectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            self.collectionView.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case .ended:
            self.collectionView.endInteractiveMovement()
        default:
            self.collectionView.cancelInteractiveMovement()
        }
    }
}

let vc = MotivatorsViewController(collectionViewLayout: MotivatorLayout())
vc.preferredContentSize = CGSize(width: 668, height: 375)
PlaygroundPage.current.liveView = vc




