//
//  DepartmentType.swift
//  MotivatorsApp
//
//  Created by Bluemm, Steffen on 10.12.19.
//  Copyright © 2019 Bluemm, Steffen. All rights reserved.
//

import Foundation

enum DepartmentType: String, CaseIterable {
    case development, design, finance, hr, marketing, sales
}
