//
//  EmployeesHelper.swift
//  MotivatorsApp
//
//  Created by Bluemm, Steffen on 10.12.19.
//  Copyright © 2019 Bluemm, Steffen. All rights reserved.
//

import Foundation

enum EmployeesHelper {
    static func generateEmployees() -> [Employee] {
        var employees: [Employee] = []
        
        employees.append(Employee(firstName: "Sarah", lastName: "Müller", department: .finance))
        employees.append(Employee(firstName: "Thorsten", lastName: "Maier", department: .design))
        employees.append(Employee(firstName: "Nils", lastName: "Lauer", department: .development))
        
        return employees
    }
}
