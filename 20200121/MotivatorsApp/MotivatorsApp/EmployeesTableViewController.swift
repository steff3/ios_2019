//
//  EmployeesTableViewController.swift
//  MotivatorsApp
//
//  Created by Bluemm, Steffen on 10.12.19.
//  Copyright © 2019 Bluemm, Steffen. All rights reserved.
//

import UIKit

class EmployeesTableViewController: UITableViewController {
    
    var items: [Employee]?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.navigationItem.title = "Employees"
        
        items = EmployeesHelper.generateEmployees()
        self.tableView.reloadData()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items?.count ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmployeeCell", for: indexPath)

        // Configure the cell...
        if let employee = items?[indexPath.row] {
            cell.detailTextLabel?.text = employee.department.rawValue.capitalized
            cell.textLabel?.text = employee.fullName
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let employee = items?[indexPath.row] {
            print(employee.fullName)
            
        }
        
        self.performSegue(withIdentifier: "DetailSegue", sender: tableView.cellForRow(at: indexPath))
    }
    


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if let sender = sender as? UITableViewCell,
            let indexPath = self.tableView.indexPath(for: sender),
            let employee = items?[indexPath.row],
            let viewController = segue.destination as? AddEmployeeViewController {
            viewController.employee = employee
        }
    }
    

}
