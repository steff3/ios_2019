//
//  Employee.swift
//  MotivatorsApp
//
//  Created by Bluemm, Steffen on 10.12.19.
//  Copyright © 2019 Bluemm, Steffen. All rights reserved.
//

import Foundation

struct Employee {
    let id: String
    let firstName: String
    let lastName: String
    let department: DepartmentType
    
    var fullName: String {
        return "\(firstName) \(lastName)"
    }
    
    init(firstName: String, lastName: String, department: DepartmentType) {
        self.firstName = firstName
        self.lastName = lastName
        self.department = department
        self.id = UUID().uuidString
    }
}
