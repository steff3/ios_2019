import UIKit

public class MotivatorLayout: UICollectionViewFlowLayout {
    public override func prepare() {
        super.prepare()
        
        guard let collectionView = collectionView else {
            return
        }
        
        let availableHeight = collectionView.bounds.inset(by: collectionView.layoutMargins).height
        
        self.itemSize = CGSize(width: 120, height: availableHeight - 20)
        self.scrollDirection = .horizontal
        self.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom:10, right: 10)
    }
}

