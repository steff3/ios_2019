//
//  Motivator.swift
//  MotivatorsApp
//
//  Created by Bluemm, Steffen on 12.11.19.
//  Copyright © 2019 Bluemm, Steffen. All rights reserved.
//

import UIKit

class Motivator {
    let type: MotivatorType
    let name: String
    var impactOffset: CGFloat = 0.0
    var impactIndex: UInt8
    
    init(type: MotivatorType, index: Int) {
        self.type = type
        name = type.rawValue.capitalized
        if UInt8.isInRange(value: index) {
            impactIndex = UInt8(index)
        } else {
            fatalError("the index is expected to be in the range between 0 and 255")
        }
    }
}

extension Motivator {
    
    func increaseImpactIndex(by: UInt8 = 1) {
        if impactIndex <= (UInt8.max - by) {
            impactIndex += by
        } else {
            fatalError("the index is expected to stay in the range between 0 and 255")
        }
    }
    
    func decreaseImpactIndex(by: UInt8 = 1) {
        if impactIndex - by >= UInt8.min {
            impactIndex -= by
        } else {
            fatalError("the index is expected to stay in the range between 0 and 255")
        }
    }
}


extension UInt8 {
    static func isInRange(value: Int) -> Bool {
        return value >= UInt8.min && value <= UInt8.max
    }
}
