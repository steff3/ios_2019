//
//  AddEmployeeViewController.swift
//  MotivatorsApp
//
//  Created by Bluemm, Steffen on 10.12.19.
//  Copyright © 2019 Bluemm, Steffen. All rights reserved.
//

import UIKit

class AddEmployeeViewController: UIViewController {

    weak var editingEntryField: UITextField?
    @IBOutlet weak var firstNameEntry: UITextField!
    @IBOutlet weak var lastNameEntry: UITextField!
    @IBOutlet weak var departmentPicker: UIPickerView!
    @IBOutlet weak var photoView: UIImageView!
    
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var departmentLabel: UILabel!
    @IBOutlet weak var departTop: NSLayoutConstraint!
    
    var employee: Employee?
    
    let departments = DepartmentType.allCases.map { $0.rawValue.capitalized }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //departTop.pr
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let employee = self.employee else {
            return
        }
        
        firstNameEntry.text = employee.firstName
        lastNameEntry.text = employee.lastName
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard let employee = self.employee else {
            return
        }
        
        departmentPicker.selectRow(departments.firstIndex(of: employee.department.rawValue.capitalized) ?? 0, inComponent: 0, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension AddEmployeeViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return departments.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return departments[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
}

extension AddEmployeeViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.editingEntryField = textField
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case firstNameEntry:
            print("first name is \(textField.text!)")
        case lastNameEntry:
            print("last name is \(textField.text!)")
        default:
            break
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case firstNameEntry:
            print("first name is \(textField.text!)")
        case lastNameEntry:
            print("last name is \(textField.text!)")
        default:
            break
        }
        return true
    }
}
