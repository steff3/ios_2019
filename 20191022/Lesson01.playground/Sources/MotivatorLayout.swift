import UIKit

public class MotivatorLayout: UICollectionViewFlowLayout {
    public override func prepare() {
        super.prepare()
        
        self.itemSize = CGSize(width: 120, height: 120)
        self.scrollDirection = .horizontal
    }
}

