import UIKit


struct Money {
    let amount: Int
    let currency: String
    
    static func exchangeCourse() -> Decimal {
        return Decimal(string: "1.09")!
    }
}

Money.exchangeCourse()

let fiveEuros = Money(amount: 5, currency: "€")


class Car {
    let horsepower: Int
    let brand: String
    var owner: String?
    
    class var numberOfWheels: Int {
        return 4
    }
    
    init(horsepower: Int, brand: String) {
        self.brand = brand
        self.horsepower = horsepower
    }
}

Car.numberOfWheels

let myCar = Car(horsepower: 180, brand: "BMW")

myCar.owner = "Hans"
print(myCar.owner!)

if let name = myCar.owner {
    print(name)
}


class User {
    let firstName: String
    let lastName: String
    var fullName: String {
        return "\(firstName) \(lastName)"
    }
    
    init(firstName: String, lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
    }
}

let myUser = User(firstName: "Hans", lastName: "Maier")

myUser.firstName
myUser.lastName
myUser.fullName
