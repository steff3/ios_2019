//
//  MotivatorCollection.swift
//  MotivatorsApp
//
//  Created by Bluemm, Steffen on 03.12.19.
//  Copyright © 2019 Bluemm, Steffen. All rights reserved.
//

import Foundation

struct MotivatorCollection {
    let motivators: [Motivator]
    let createdAt: Date
    
    init(motivators: [Motivator]) {
        self.motivators = motivators
        self.createdAt = Date()
    }
}

extension MotivatorCollection {
    static func baseCollection() -> [Motivator] {
        let types = MotivatorType.allCases
        
        var motivators: [Motivator] = []
        for (index, type) in types.enumerated() {
            motivators.append(Motivator(type: type, index: index))
        }
        
        return motivators
    }
    
    
    
    func extractSorting() -> [(type: MotivatorType, index: Int)] {
        return motivators.map { (type: $0.type, index: Int($0.impactIndex) )}
    }
}































/*
 func sortedBaseCollection() -> [Motivator] {
 var baseSorted = type(of: self).baseCollection()
 let sorting = extractSorting()
 baseSorted.sort { (m1, m2) -> Bool in
 let indexM1 = sorting.first(where: { e -> Bool in
 e.type == m1.type
 })!.index
 let indexM2 = sorting.first(where: { e -> Bool in
 e.type == m2.type
 })!.index
 
 return indexM1 < indexM2
 }
 
 return baseSorted
 }
 
 */
