import UIKit
import PlaygroundSupport

///
///     Our custom UICollectionViewController class
///     - hold a collection of motivators
///     - instruct the UICollectionView to display them correctly
///
class MotivatorsViewController: UICollectionViewController {
    /// our collection of motivators
    private var items: [String] = ["Acceptance", "Status", "Curiosity"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// tell the collectionView which type of cellView to use, as well as the reuse-Id
        self.collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
    }
    
    /// tell the collectionView how many items to display
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    /// tell the collectionView what to display for each item
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        /// ask the collectionView for a reusable cellView by reuse-Id
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        cell.backgroundColor = .blue
        
        //cell.setLabel(text: "Hallo")
        
        return cell
    }
}

let vc = MotivatorsViewController(collectionViewLayout: UICollectionViewFlowLayout())
PlaygroundPage.current.liveView = vc




